const connection = require('../config/dbconnection')
const Cerveza = require('../models/Cerveza')

const index = (req, res) => {
  // console.log('lista de cervezas')
  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
}

const show = function (req, res) {
  // console.log('detalle de cervezas')
  const id = req.params.id
  Cerveza.find(id, (status, data) => {
    res.status(status).json(data)
  })
}

const destroy = (req, res) => {
  const id = req.params.id
  Cerveza.destroy(id, (status, data) => {
    res.status(status).json(data)
  })
}

const store = (req, res) => {
  const obj = new Object()
  obj.name = req.body.name
  obj.description = req.body.description
  obj.alcohol = req.body.alcohol
  obj.container = req.body.container
  obj.price = req.body.price
  Cerveza.store(obj, (status, data) => {
    res.status(status).json(data)
  })
  // res.json({ mensaje: ` cerveza almacenada` })
}

const update = (req, res) => {
  Cerveza.update(
    req.body.name,
    req.body.container,
    req.params.id,
    (status, data) => {
      res.status(status).json(data)
    }
  )
}

module.exports = {
  index,
  show,
  destroy,
  store,
  update
}
