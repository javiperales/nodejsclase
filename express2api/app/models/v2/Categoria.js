const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const categoriaSchema = new Schema({
	nombre: {
		type: String,
		required: true,
		maxlength: 20
	},

	description: {
		type: String,
		maxlength: 255
	},
	created: {
		type: Date,
		default: Date.now
	}
});

const Categoria = mongoose.model("Categoria", categoriaSchema);

module.exports = Categoria;
