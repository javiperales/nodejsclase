const Categoria = require("../../models/v2/Categoria");
const { ObjectId } = require("mongodb");

const index = (req, res) => {
	Categoria.find((err, categorias) => {
		if (err) {
			return res.status(500).json({
				message: "Error obteniendo la categoria"
			});
		}
		return res.json(categorias);
	});
};

const create = (req, res) => {
	const categoria = new Categoria(req.body);
	categoria.save((err, categoria) => {
		if (err) {
			return res.status(400).json({
				message: "Error al guardar la categoria",
				error: err
			});
		}
		return res.status(201).json(categoria);
	});
};

const update = (req, res) => {
	const id = req.params.id;
	Categoria.findOne({ _id: id }, (err, categoria) => {
		if (!ObjectId.isValid(id)) {
			return res.status(404).send();
		}
		if (err) {
			return res.status(500).json({
				message: "Se ha producido un error al guardar la categoria",
				error: err
			});
		}
		if (!ObjectId.isValid(id)) {
			return res.status(404).send();
		}
		if (!categoria) {
			return res.status(404).json({
				message: "No hemos encontrado la categoria"
			});
		}

		Object.assign(categoria, req.body);

		categoria.save((err, categoria) => {
			if (err) {
				return res.status(500).json({
					message: "Error al guardar la categoria"
				});
			}
			if (!categoria) {
				return res.status(404).json({
					message: "No hemos encontrado la categoria"
				});
			}
			return res.json(categoria);
		});
	});
};

const show = (req, res) => {
	const id = req.params.id;
	Categoria.findOne({ _id: id }, (err, categoria) => {
		if (!ObjectId.isValid(id)) {
			return res.status(404).send();
		}
		if (err) {
			return res.status(500).json({
				message: "Se ha producido un error al obtener la categoria"
			});
		}
		if (!categoria) {
			return res.status(404).json({
				message: "No tenemos esta categoria"
			});
		}
		return res.json(categoria);
	});
};

const remove = (req, res) => {
	const id = req.params.id;

	Categoria.findOneAndDelete({ _id: id }, (err, categoria) => {
		if (!ObjectId.isValid(id)) {
			return res.status(404).send();
		}
		if (err) {
			return res.json(500, {
				message: "No hemos encontrado la categoria"
			});
		}
		if (!categoria) {
			return res.status(404).json({
				message: "No hemos encontrado la categoria"
			});
		}
		return res.json(categoria);
	});
};

module.exports = {
	index,
	create,
	update,
	show,
	remove
};
