const express = require("express");
const router = express.Router();
const productController = require("../../controllers/v2/productController.js");
//const servicejwt = require("../../services/servicejwt");
const auth = require("../../middleware/auth");

router.use(auth.auth);

//rutas
router.get("/", (req, res) => {
	console.log("ruta de productos");
	productController.index(req, res);
});

// router.get("/", auth, (req, res) => {
// 	productController.index(req, res);
// });

router.get("/:id", auth.auth, (req, res) => {
	console.log("estas en show");
	productController.show(req, res);
});

router.post("/", (req, res) => {
	console.log("estas en create");
	productController.create(req, res);
});

router.delete("/:id", (req, res) => {
	console.log("estas en delete");
	productController.remove(req, res);
});

router.put("/:id", (req, res) => {
	console.log("estas en update");
	productController.update(req, res);
});

module.exports = router;
